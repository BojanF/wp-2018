package mk.ukim.finki.wp.organizeme.service.impl

import mk.ukim.finki.wp.organizeme.model.Activity
import mk.ukim.finki.wp.organizeme.model.Client
import mk.ukim.finki.wp.organizeme.model.Project
import mk.ukim.finki.wp.organizeme.model.Task
import mk.ukim.finki.wp.organizeme.model.exceptions.TaskNotFoundException
import mk.ukim.finki.wp.organizeme.persistence.TaskRepository
import mk.ukim.finki.wp.organizeme.service.CurrentTimeService
import spock.lang.Specification
import spock.lang.Unroll

import java.time.Duration
import java.time.LocalDate
import java.time.LocalTime
import java.util.stream.Stream

import static java.util.stream.Collectors.toList

/**
 * @author Riste Stojanov
 */
class TaskServiceImplTest extends Specification {

    TaskRepository repository = Mock()
    CurrentTimeService currentTimeService = Mock()

    TaskServiceImpl service

    void setup() {

        service = new TaskServiceImpl(repository,
                currentTimeService)
    }

    @Unroll
    def "GetTaskActivities for index #index for period #period"() {


        given:

        def time = LocalTime.now()
        def date = LocalDate.now()


        currentTimeService.getCurrentDate() >> date
        currentTimeService.getCurrentTime() >> time

        repository.findAll() >> getTasks()

        when:
        def result
        def ex
        try {
            result = service.getTaskActivities(index, period)
        } catch (e) {
//            e.printStackTrace()
            ex = e
        }
        then:
        if (exception != null) {
            assert ex.getClass() == exception
        } else {
            assert result != null
            assert result.size() == resultSize
        }

        where:
        index | period | resultSize | exception
        -1    | 1      | null       | TaskNotFoundException.class
        5     | 1      | null       | TaskNotFoundException.class
        1     | -5     | 0          | null
        1     | 40     | 1          | null
        1     | 20     | 0          | null
        1     | 23     | 1          | null
        1     | 63     | 2          | null
        1     | 64     | 2          | null
    }


    List<Task> getTasks() {

        List<Task> tasks = new ArrayList<>()
        Client finki = new Client()
        finki.id = 1L
        finki.name = "FINKI"

        Project wp = new Project()
        wp.id = 1L
        wp.name = "WP"
        wp.client = finki

        Activity a1 = new Activity()
        a1.date = currentTimeService.getCurrentDate()
        a1.from = currentTimeService.getCurrentTime()
                .minusHours(1)
                .minusMinutes(3)
        a1.to = a1.from.plusMinutes(40)

        Activity a2 = new Activity()
        a2.date = a1.date
        a2.from = a1.to

        Task t1 = new Task()
        t1.title = "T1"
        t1.project = wp
        t1.totalTime = Duration.ofHours(1).plusMinutes(3).plusSeconds(3)
        t1.activity = Stream
                .of(a1, a2)
                .collect(toList())

        return Stream.of(t1, t1, t1)
                .collect(toList())


    }
}
