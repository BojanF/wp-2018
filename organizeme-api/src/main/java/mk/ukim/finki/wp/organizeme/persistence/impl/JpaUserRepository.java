package mk.ukim.finki.wp.organizeme.persistence.impl;

import mk.ukim.finki.wp.organizeme.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author Riste Stojanov
 */
public interface JpaUserRepository extends JpaRepository<User, String> {
}
