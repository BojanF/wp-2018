package mk.ukim.finki.wp.organizeme.model.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * @author Riste Stojanov
 */
@ResponseStatus(HttpStatus.BAD_REQUEST)
public class ActivityNotFoundException extends Exception {
}
