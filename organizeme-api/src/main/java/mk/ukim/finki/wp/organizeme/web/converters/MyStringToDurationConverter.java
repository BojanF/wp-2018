package mk.ukim.finki.wp.organizeme.web.converters;

import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.type.TypeFactory;
import org.springframework.core.convert.converter.Converter;

import java.time.Duration;

/**
 * @author Riste Stojanov
 */
public class MyStringToDurationConverter implements Converter<String, Duration> {

    @Override
    public Duration convert(String s) {
        String[] parts = s.split(":");
        return Duration
          .ofHours(Integer.parseInt(parts[0]))
          .ofMinutes(Integer.parseInt(parts[1]))
          .ofSeconds(Integer.parseInt(parts[2]));
    }
}
