export const dateTimeFormat = 'DD.MM.YYYY HH:mm:ss';
export const dateOnlyFormat = 'DD.MM.YYYY';
export const timeOnlyFormat = 'HH:mm:ss';

/* The time format HH is 24 hours format, while hh (which was before) was 12 hours format, and that caused issue in total time calculation */